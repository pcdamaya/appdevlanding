/*
Copyright (c) 2016 Micky
------------------------------------------------------------------
[Master Javascript]
Project: Micky
-------------------------------------------------------------------*/
(function($) {
	"use strict";
	var Micky = {
		initialised: false,
		version: 1.0,
		mobile: false,
		init: function() {
			if (!this.initialised) {
				this.initialised = true;
			} else {
				return;
			}
			/*-------------- Micky Functions Calling ---------------------------------------------------
			------------------------------------------------------------------------------------------------*/
			this.RTL();
			this.slider();
			this.navigations();
			this.smooth_scroll();
			this.contact_mail();
			this.animation();
		},
		/*-------------- Micky Functions definition ---------------------------------------------------
		---------------------------------------------------------------------------------------------------*/
		RTL: function() {
			// On Right-to-left(RTL) add class 
			var rtl_attr = $("html").attr('dir');
			if (rtl_attr) {
				$('html').find('body').addClass("rtl");
			}
		},
		slider:function(){
			
			
		},
		navigations:function () {
		//menu active on click
		$(".mkf_navigation ul li ").on("click", function(){
			$('ul li').removeClass("active");
			$(this).addClass("active");
		});
		
		$(".bullets_wrap a").on("click", function(){
			$('.bullets_wrap a').removeClass("active");
			$(this).addClass("active");
		});
		
		//menu open in mobile
		$(".menu_toggle").on ("click", function(){
			$(this).toggleClass("toggle_open");
			$(".mkf_navigation").slideToggle();
		});
		
		},
		
		//smooth scroll
		smooth_scroll:function () {
			$('a[href]').click(function(){
				$('html, body').animate({
				scrollTop: $( $.attr(this, 'href') ).offset().top
				}, 1000);
				return false;
			});
		},
		//contact mail
		contact_mail:function (){
			$('#submit_btn').on('click', function(){
				var un=$('#username').val();
				var em=$('#useremail').val();
				var ucountry=$('#usercountry').val();
				var umessage=$('#message').val();
				
				$.ajax({
					type: "POST",
					url: "contact_ajaxmail.php",
					data: {
						'username':un,
						'useremail':em,
						'usercountry':ucountry,
						'umessage':umessage,
						},
					success: function(msg) {
						var full_msg=msg.split("#");
						if(full_msg[0]=='1'){
							$('#username').val("");
							$('#useremail').val("");
							$('#usercountry').val("");
							$('#message').val("");
							$('#err').html( full_msg[1] );
						}
						else{
							$('#username').val(un);
							$('#useremail').val(em);
							$('#usercountry').val(ucountry);
							$('#message').val(umessage);
							$('#err').html( full_msg[1] );
						}
					}
				});
			});
		},
		//animation
		animation:function(){
			new WOW().init();
		}
		
   };
	Micky.init();
	
	//load event
	$(window).load(function() {
		 $(".micky_preloader").delay(500).fadeOut("slow");
		
	});

})(jQuery);